[1, 2, 3, 4, 5]
# => [1, 2, 3, 4, 5]

[1 , :foo, 2, "bar", 3, true]
# => [1, :foo, 2, "bar", 3, true]

%w[foo bar baz] # => ["foo", "bar", "baz"]
%w(foo bar baz) # => ["foo", "bar", "baz"]
%w{'foo' bar baz} # => ["foo", "bar", "baz"]

a = "foo"
b = "bar"
c = "baz"

%W[foo bar #{c}] # => ["foo", "bar", "baz"]
%W(foo #{b} baz) # => ["foo", "bar", "baz"]
%W{#{a} bar baz} # => ["foo", "bar", "baz"]

%i[foo bar baz] # => [:foo, :bar, :baz]

arr = []
arr << "bar"
arr.push(:foo)
# ["bar", :foo]
arr.delete("bar")
arr.delete(:foo)
# []

arr[0] # => "bar"

arr[1] # => :foo

arr[0, 1] # => ["bar", :foo]

arr.slice(1) # => :foo

arr.slice(0, 7) # => ["bar", :foo]

arr.empty? # => false

arr.size # => 2

arr.count # => 2

arr.empty? # => true

arr.each do |item|
  puts item
end
# bar
# foo

