"string".class
# => String

"OK 👌".encoding
# => #<Encoding:UTF-8>

'string'
# => "string"

'line1\nline1'
# => line1\\nline1

'line1\nline1'
# => line1\\nline1

'line1\'line1'
# => "line1'line1"

"line1\nline2"
# => "line1\nline2"

"\"convinient\""
# => "\"convinient\""

'"confinient"'
# => "\"convinient\""

%{string}
# => "string"

%(string)
# => "string"

%[string]
# => "string"

%{"'\n'"
'"}
# => "\"'\n'\"\n'\""

"multiline
string"
# => "multiline\nstring"

<<-SQL

SELECT * FROM users
WHERE users.id = 2

SQL
#=> "\nSELECT * FROM users\nWHERE users.id = 2\n\n"

'foo' + 'bar'
# => "foobar"

'bang! ' * 3
# => "bang! bang! bang! "

a = 1
b = 2
"#{a} + #{b} = #{a + b}"
# => "1 + 2 = 3"

%{#{a} + #{b} = #{a + b}}
# => "1 + 2 = 3"

<<-INTERPOLATION
#{a} + #{b} = #{a + b}
INTERPOLATION
# => "1 + 2 = 3\n"
