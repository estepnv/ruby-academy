puts self
# main
puts self.class
# Object

def do_a
  puts "doing A"
  puts self

  def self.do_b
    puts "doing B"
  end
end

self.do_a
do_b

def self.do_c
  puts "doing C"
end

def do_c
  puts "doing C"
end

define_method(:do_c) do
  puts "doing C"
end

define_singleton_method(:do_c) do
  puts "doing C"
end
