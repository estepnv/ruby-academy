EXIT_CODE = "1"
ENTER_NAME_CODE = "2"
PRINT_NAME_CODE = "3"

def get_input(prompt = "")
  print prompt
  gets.chomp
end

def print_name(name)
  puts "Your name: #{name}"
end

def print_menu
  puts("Enter #{PRINT_NAME_CODE} to print your name")
  puts("Enter #{ENTER_NAME_CODE} to enter your name")
  puts("Enter #{EXIT_CODE} for exit")
end

def main
  input, state = nil, nil

  loop do
    print_menu
    input = get_input("Enter command: >")

    case input
    when ENTER_NAME_CODE
      state = get_input("Enter name: >")
    when PRINT_NAME_CODE
      print_name(state)
    when EXIT_CODE
      break
    end
  end

  puts "All done"
end

main
