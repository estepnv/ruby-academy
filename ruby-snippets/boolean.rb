true.class
# => TrueClass

false.class
# => FalseClass

true && false
# => false

true || false
# => true
