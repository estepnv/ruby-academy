str = "OK"

def str.foobarize
  "FOO BAR #{self}"
end

str.foobarize
# => "FOO BAR OK"

str.define_singleton_method(:barfoorize) do
  "BAR FOO #{self}"
end

str.barfoorize
# => "BAR FOO OK"
