puts "hello", "world"
$stdout.puts "hello", "world"
STDOUT.puts "hello", "world"
# hello
# world

print "hello", "world"
$stdout.print "hello", "world"
STDOUT.print "hello", "world"
# helloworld

gets
$stdin.gets
STDIN.gets
# > helloworld
# => "helloworld\n"

$stdin.readline
STDIN.readline
# > helloworld
# => "helloworld\n"
