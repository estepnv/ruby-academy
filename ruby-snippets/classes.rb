
Vector = Class.new do
  def initialize(x, y)
    @x = x
    @y = y
  end
end

Vector.class # => Class

# # # # # # # # # # # # # # # # # # # #

class Vector
  def initialize(x, y)
    @x = x
    @y = y
  end
end

Vector.class # => Class
Vector.new(7, 2) # => #<Vector:0x00007fe0f300ab50 @x=3, @y=2>
Vector.new(1, 5) # => #<Vector:0x00007fe0f18a9ba0 @x=1, @y=5>
Vector.new(1,2).x
# => NoMethodError (undefined method `x' for #<Vector:0x00007ff01b0e2460 @x=1, @y=2>)

# # # # # # # # # # # # # # # # # # # #

class Counter
  def initialize
    @n = 0
  end

  def value
    @n
  end

  def increment
    @n = @n + 1
    nil
  end
end

counter = Counter.new

counter.increment
counter.increment
counter.increment

counter.value # => 3

# # # # # # # # # # # # # # # # # # # #

class Counter
  def initialize
    self.value = 0
  end

  def value
    @value
  end

  def value=(val)
    @value = val
  end

  def increment
    self.value = value + 1
    nil
  end
end

counter = Counter.new

counter.increment
counter.increment
counter.increment

counter.value # => 3

# # # # # # # # # # # # # # # # # # # #

class Counter
  attr_accessor :value

  def initialize
    self.value = 0
  end

  def increment
    self.value = value + 1
    nil
  end
end

# # # # # # # # # # # # # # # # # # # #

class Vector
  attr_reader :x, :y

  def initialize(x, y)
    @x = x.to_f
    @y = y.to_f
  end

  def inspect
    "{#{x},#{y}}"
  end
end

# # # # # # # # # # # # # # # # # # # #

module UnderscoreReader
  def _attr_reader(*names)
    names.each do |name|
      define_method("_#{name}") do
        instance_variable_get "@#{name}"
      end
    end
  end
end

class Vector
  extend UnderscoreReader

  _attr_reader :x, :y
end

Vector.extend(UnderscoreReader)
Vector._attr_reader(:x, :y)

vector = Vector.new(1, 2)
vector._x
vector._y

Vector.new(1, 2) # => {1.0,2.0}

# # # # # # # # # # # # # # # # # # # #

module Coordinates
  def x; @x; end
  def y; @y; end

  def x=(val); @x = val; end
  def y=(val); @y = val; end
end

class Vector
  include Coordinates
end

Vector.include(Coordinates)

vector = Vector.new
vector.x = 1
vector.y = 2

# # # # # # # # # # # # # # # # # # # #

class Vector
  def sum(vector)
    Vector.new(x + vector.x, y + vector.y)
  end

  alias + sum

  def subtract(vector)
    Vector.new(x - vector.x, y - vector.y)
  end

  alias - subtract
end

Vector.new(1,1) + Vector.new(1,1) # => {2.0, 2.0}
Vector.new(-1,3) - Vector.new(3,3) # => {-4.0, 0.0}

# # # # # # # # # # # # # # # # # # # #

class Vector
  def self.unit
    Vector.new(1, 1)
  end
end

Vector.unit # => {1.0, 1.0}

# # # # # # # # # # # # # # # # # # # #

class Vector < Numeric

  def self.unit
    Vector.new(1, 1)
  end

  attr_reader :x, :y

  def initialize(x, y)
    @x = x.to_f
    @y = y.to_f
  end

  def sum(vector)
    Vector.new(x + vector.x, y + vector.y)
  end

  alias + sum

  def subtract(vector)
    Vector.new(x - vector.x, y - vector.y)
  end

  alias - subtract

  def multiply(vector)
    coerced, numeric = coerce(self, vector)

    if coerced.is_a?(Float)
      Vector.new(x * numeric, y * numeric)
    else
      Vector.new(x * numeric.x, y * numeric.y)
    end
  end

  def divide(vector)
    multiply(1 / vector)
  end

  alias * multiply

  def length
    (x**2 + y**2)**0.5
  end

  def to_f
    length.to_f
  end

  def coerce(a)
    if a.is_a?(Vector)
      [a, self]
    else
      [a.to_f, self.to_f]
    end
  end

  def inspect
    "{#{x}, #{y}}"
  end
end

# # # # # # # # # # # # # # # # # # # #

class Vector
  attr_accessor :x, :y

  def initialize(x, y)
    @x = x
    @y = y
  end
end

class Shape
  attr_accessor :location

  def initialize(location)
    self.location = location || Vector.new(0, 0)
  end

  def draw
    raise NotImplementedError
  end

  def move(point)
    self.location = point
    draw
  end
end

class Circle < Shape
  attr_accessor :radius
  DOT = "**"
  SPACE = "  "
  THIKNESS = 0.5

  def initialize(location, radius)
    super(location)
    self.radius = radius
  end

  def draw
    rout = radius + THIKNESS
    rin = radius - THIKNESS

    y = radius.to_f
    location.y.times { puts }
    while y >= (-1 * radius.to_f)
      location.x.times { print SPACE}
      x = (-1 * radius.to_f)
      while x < rout
        val = x * x + y * y

        if val >= rin * rin && val <= rout * rout
          print DOT
        else
          print SPACE
        end
        x = x + 1
      end
      print "\n"
      y = y - 1
    end
  end
end

circle = Circle.new(Vector.new(5,5), 5)
# => #<Circle:0x00007f9406b24fc0 @location=#<Vector:0x00007f9406b24fe8 @x=5, @y=5>, @radius=5>
circle.draw
#
#
#
#
#
#                **********
#              **          **
#            **              **
#          **                  **
#          **                  **
#          **                  **
#          **                  **
#          **                  **
#            **              **
#              **          **
#                **********

class Rect < Shape
  attr_accessor :width, :height

  def initialize(location, width, height)
    super(location)
    self.width = width.to_f
    self.height = height.to_f
  end

  def draw
    # implementation
    location.y.to_i.times { puts }

    location.x.to_i.times { print SPACE }
    width.to_i.times { print DOT }
    puts
    (height - THIKNESS * 2).to_i.times {
      location.x.to_i.times { print SPACE }
      print DOT
      (width - THIKNESS * 3).to_i.times { print SPACE }
      print DOT
      puts
    }
    location.x.to_i.times { print SPACE }
    width.to_i.times { print DOT }

    puts
  end
end
rect = Rect.new(Vector.new(5,5), 5, 5)
# => #<Rect:0x00007f9409083488 @height=5.0, @location=#<Vector:0x00007f94090834b0 @x=5, @y=5>, @width=5.0>
rect.draw
#
#
#
#
#
#           **********
#           **      **
#           **      **
#           **      **
#           **      **
#           **********

shapes = [rect, circle]
shapes.each { |shape| shape.draw }
