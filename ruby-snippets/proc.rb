greeter = Proc.new { |username| puts "helloworld #{username}" }
greeter = proc { |username| puts "helloworld #{username}" }
greeter.class # => Proc


greeter.call("John Doe")
greeter["John Doe"]
greeter.("John Doe")
# helloworld John Doe

greeter.call
# helloworld

def base_pow(base)
  Proc.new { |n| base ** n }
end

base_2_pow = base_pow(2)
base_10_pow = base_pow(10)

base_2_pow.call(3) # => 8
base_2_pow.call(16) # => 65536

base_10_pow.call(2) # => 100
base_10_pow.call(6) # => 1000000

sum = -> (a, b) { a + b }

sum = lambda do
  a + b
end

sum.class # => Proc

sum.(1,2) # => 3
sum.call
# ArgumentError (wrong number of arguments (given 0, expected 2))


def _select(list, &block)
  res = []

  for i in 0...list.size
    res << list[i] if block.call(list[i])
  end

  res
end

_select([1,2,3,4]) { |n| n % 2 == 0 } # => [2, 4]
