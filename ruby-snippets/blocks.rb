def dumbmethod
  puts "before block invocation"
  yield
  puts "after block invocation"
end

dumbmethod { puts "helloworld" }
# before block invocation
# helloworld
# after block invocation


def runner(print)
  yield(print)
end

runner("helloworld") do |arg|
  puts(arg)
end
# helloworld

def three_times
  yield 1
  yield 2
  yield 3
end

three_times do |n|
  puts "this is #{n}"
end
# this is 1
# this is 2
# this is 3

def _select(list)
  res = []

  for i in 0...list.size # [0 .. size - 1 ]
    res << list[i] if yield(list[i])
  end

  res
end

_select([1,2,3,4]) { |n| n % 2 == 0 } # => [2, 4]
_select([1,2,3,4]) { |n| n % 2 == 1 } # => [1, 3]
_select([1,2,3,4]) { _1 % 2 == 1 } # => [1, 3]
