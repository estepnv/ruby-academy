
input = nil

loop do
  print("Enter a number (-1 quit) > ")
  input = gets.to_i

  puts("You've entered #{input}")

  break if input == -1
end

puts "All done"

# ➜  ~ ruby helloworld.rb
# Enter a number (-1 quit) > 1
# You've entered 1
# Enter a number (-1 quit) > 2
# You've entered 2
# Enter a number (-1 quit) > 2356
# You've entered 2356
# Enter a number (-1 quit) > p23o
# You've entered 0
# Enter a number (-1 quit) > 982739857236
# You've entered 982739857236
# Enter a number (-1 quit) > -1
# You've entered -1
# All done
