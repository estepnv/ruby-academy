#!/usr/bin/ruby

# ./subroutine.rb

Subroutine = Module.new

def Subroutine.start
  puts "I do something useful"
end

Subroutine.start
# I do something useful


#!/usr/bin/ruby

# ./subroutine.rb

module Subroutine
  def Subroutine::start
    puts "I do something useful"
  end
end

Subroutine::start
# I do something useful

#!/usr/bin/ruby

# ./subroutine.rb

module Subroutine
  def self.start
    puts "I do something useful"
  end
end

Subroutine.start
# I do something useful

# ./subroutine.rb

module Subroutine
end

# ./subroutine/method_1.rb

def Subroutine::method_1
  puts "I'm method 1"
end

def Subroutine::method_2
  puts "I'm method 2"
end


Subroutine.start
# I do something useful

# subroutine_1.rb

module SubroutineOne
  def SubroutineOne::start
    prepared_data = prepare_data({ a: 1, b: 2, c: 3 })
    write_data(prepared_data)
  end

  def SubroutineOne::prepare_data(hash)
    hash.map { |k, v| "#{k} => #{v}\n" }.join
  end

  def SubroutineOne::write_data(data)
    File.open("./data", 'w+') { |f| f.write(data) }
  end
end

# subroutine_2.rb

module SubroutineTwo
  def SubroutineTwo::start
    data = read_file
    display_data(data)
  end

  def SubroutineTwo::read_file
    File.read("./data")
  end

  def SubroutineTwo::display_data(data)
    puts data
  end
end

require 'subroutine_1'
require 'subroutine_2'

SubroutineOne.start
SubroutineTwo.start
# a => 1
# b => 2
# c => 3

module Lib
  module SubroutineOne
    # ..
  end

  module SubroutineTwo
    # ..
  end
end

Lib::SubroutineOne.start
Lib::SubroutineTwo.start
# a => 1
# b => 2
# c => 3

module Colors
  ALL = [
    RED = '#ff0000',
    GREEN = '#00ff00',
    BLUE = '#0000ff'
  ]
end

Colors::RED   # => "#ff0000"
Colors::GREEN # => "#00ff00"
Colors::BLUE  # => "#0000ff"
Colors::ALL   # => ["#ff0000", "#00ff00", "#0000ff"]

module Foobarizer
  def foobarize
    "foobar#{to_s}"
  end
end

str = "hello"
str.extend(Foobarizer)
str.foobarize # => "foobarhello"


module FooBar
end

module Foo
  def foo; puts 'foo'; end
end

module Bar
  def bar; puts 'bar'; end
end

FooBar.extend(Foo)
FooBar.extend(Bar)

FooBar.foo # => foo
FooBar.bar # => bar


module FooBar
end

module Foo
  def foo; puts 'foo'; end
end

module Bar
  def bar; puts 'bar'; end
end

FooBar.include(Foo)
FooBar.include(Bar)

extend(FooBar)
foo # => foo
bar # => bar

module SerializableValue
end

module SerializableValue::ClassMethods
  attr_accessor :coder

  def serialize_with(coder)
    self.coder = coder
  end
end

module SerializableValue::InstanceMethods
  attr_accessor :raw_value

  def value
    return if !raw_value
    self.class.coder.load(raw_value)
  end

  def value=(val)
    self.raw_value = self.class.coder.dump(val)
  end
end

def SerializableValue::included(base)
  base.extend(ClassMethods)
  base.include(InstanceMethods)
end

class YamlValueHost
  include SerializableValue

  serialize_with YAML
end

yaml_v = YamlValueHost.new
yaml_v.value = { msg: 'helloworld' }
yaml_v.raw_value # => "---\n:msg: helloworld\n"
yaml_v.value # => => {:msg=>"helloworld"}

class JsonValueHost
  include SerializableValue

  serialize_with JSON
end

yaml_v = JsonValueHost.new
yaml_v.value = { msg: 'helloworld' }
yaml_v.raw_value # => "{\"msg\":\"helloworld\"}"
yaml_v.value # => => {:msg=>"helloworld"}
