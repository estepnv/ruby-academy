
if 2 % 2 == 0
  puts "even"
end
# even

if 3 % 2 == 1
  puts "odd"
end
# odd

puts "even" if 6 % 2 == 0
# even
puts "even" unless 6 % 2 == 1
# odd

if 8 % 2 == 0
  puts "even"
else
  puts "odd"
end
# even

unless 12 % 2 == 0
  puts "odd"
else
  puts "even"
end
# even

puts(
  if 6 % 2 == 0
    "even"
  else
    "odd"
  end
)

if 64 % 2 == 0 then "even" else "odd" end # => "even"
52 % 2 == 0 ? "odd" : "even" # => "even"

30 % 2 == 0 and "even" or "odd" # => "even"

true and "1" or "0" # => "1"
false and "1" or "0" # => "0"
"ok" and "1" or "0" # => "1"
nil and "1" or "0" # => "0"


READ = 0
WRITE = 1
WAIT = 2

control_flag = READ
case control_flag
when READ
  read_data
when WRITE
  write_data
when WAIT
  sleep(1)
end
