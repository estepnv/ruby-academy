sum = 0

while true
  input = gets.to_i

  next if input == 0
  break if input == -1

  sum += input
end

puts sum


res = 1
input = 6

until input == 0
  res *= input
  input -=1
end

puts res
# 720

input = gets.to_i

planets = ["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"]

for planet in planets
  puts planet
end

planets = ["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"]

planets.each
# => #<Enumerator: ["Mercury", "Venus" .. ]:each>

planets.each do |planet|
  puts planet
end



loop
# => #<Enumerator: main:loop>

sum = 0

loop do
  input = gets.to_i

  next if input == 0
  break if input == -1

  sum += input
end
