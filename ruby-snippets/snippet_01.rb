
input = nil

loop do
  print "Enter a number (-1 quit) > "
  input = gets.to_i

  puts "You've entered #{input}"

  break if input == -1
end

puts "All done"

