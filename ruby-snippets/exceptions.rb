begin
  person  = { first_name: 'John', last_name: "Doe", age: 54 }
  f = File.open("foo", "w")
  f.puts(person[:first_name])
  f.puts(person[:last_name])
  f.puts(person[:age])
  f.puts(person[:age][:age])
rescue => e
  puts "error occurred: #{e}"
ensure
  f.close
end

def foo
  raise("oops")
rescue StandardError => e
  puts "error occurred: #{e.message}"
  fail("not again")
ensure
  puts "have a nice day"
end

# foo
# error occurred: oops
# have a nice day
# RuntimeError (not again)
