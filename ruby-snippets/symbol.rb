def foo_str
  puts "foo".object_id
end

def bar_str
  puts "foo".object_id
end

foo_str
# 240
bar_str
# 260
puts "foo".object_id
# 280

def foo_sym
  puts :foo.object_id
end

def bar_sym
  puts :foo.object_id
end

foo_sym
# 2013148
bar_sym
# 2013148
puts :foo.object_id
# 2013148
