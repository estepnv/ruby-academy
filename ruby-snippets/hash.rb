grades = { "Jane Doe" => 10, "Jim Doe" => 6 }
grades["John Doe"] = 7
puts grades["John Doe"]
# 7
puts grades
# { "Jane Doe" => 10, "Jim Doe" => 6, "John Doe" => 7 }

{ 1 => 1, "2" => 2, :foo => "bar", 7.3 => [] }
# => {1=>1, "2"=>2, :foo=>"bar", 7.3=>[]}

grades = Hash.new(-1)
grades["John Doe"]
# => -1
grades.keys
# => []

grades = Hash.new { |hash, key| hash[key] = -1 }
grades["John Doe"]
# => -1
grades.keys
# => ["John Doe"]


student_1 = "John Doe"
student_2 = "Jane Doe"
student_3 = "Jim Doe"

score_1 = 7
score_2 = 9
score_3 = 3

grades = {
  student_1 => score_1,
  student_2 => score_2,
  student_3 => score_3
}

grades["John Doe"] == score_1
# => true

person = { :first_name => "John", :last_name => "Doe" }
# => {:first_name=>"John", :last_name=>"Doe"}

person = { first_name: "John", last_name: "Doe" }
# => {:first_name=>"John", :last_name=>"Doe"}

person = { :first_name => "John", last_name: "Doe" }
# => {:first_name=>"John", :last_name=>"Doe"}

grades = { "John Doe": 4, "Jim Doe": 9 }
# => {:"John Doe"=>4, :"Jim Doe"=>9}

def print_hash(list)
  list.each { print _1, " => ", _2, "\n" }
end

print_list({ :first_name => "John", last_name: "Doe" })
# => OK
print_list(:first_name => "John", last_name: "Doe")
# => OK
print_list :first_name => "John", last_name: "Doe"
# => OK

# print_list { :first_name => "John", last_name: "Doe" }
# => WASTED

def print_person(params)
  last_name = params.fetch(:last_name)
  first_name = params.fetch(:first_name)
  puts "First name: #{first_name}; Last name: #{last_name}"
end

print_person(first_name: "John", last_name: "Doe")
# First name: John; Last name: Doe

def print_person(first_name:, last_name:)
  puts "First name: #{first_name}; Last name: #{last_name}"
end

print_person(first_name: "John", last_name: "Doe")
# First name: John; Last name: Doe
